<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/article')]
class ArticleController extends AbstractController
{
    #[Route('/', name: 'app_article_index', methods: ['GET'])]
    public function index(Request $request, ArticleRepository $articleRepository): Response
    {
        if (!$request->getSession()->get('user_id')) {
            return $this->redirectToRoute('home');
        }

        if ($request->getSession()->get('user_pseudo') != 'Minh') {
            return $this->redirectToRoute('home');
        }

        return $this->render('article/index.html.twig', [
            'articles' => $articleRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_article_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ArticleRepository $articleRepository, CategorieRepository $categorieRepository): Response
    {
        if (!$request->getSession()->get('user_id')) {
            return $this->redirectToRoute('home');
        }

        if ($request->getSession()->get('user_pseudo') != 'Minh') {
            return $this->redirectToRoute('home');
        }

        $article = new Article();
        $form = $this->createForm(ArticleType::class, $article, [
            'categories' => $categorieRepository->findAll(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->save($article, true);

            return $this->redirectToRoute('admin', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/new.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_article_show', methods: ['GET'])]
    public function show(Article $article): Response
    {
        return $this->render('article/show.html.twig', [
            'article' => $article,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_article_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Article $article, ArticleRepository $articleRepository, CategorieRepository $categorieRepository): Response
    {
        if (!$request->getSession()->get('user_id')) {
            return $this->redirectToRoute('home');
        }

        if ($request->getSession()->get('user_pseudo') != 'Minh') {
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(ArticleType::class, $article, [
            'categories' => $categorieRepository->findAll(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $articleRepository->save($article, true);

            return $this->redirectToRoute('admin', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('article/edit.html.twig', [
            'article' => $article,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_article_delete', methods: ['POST'])]
    public function delete(Request $request, Article $article, ArticleRepository $articleRepository): Response
    {
        if (!$request->getSession()->get('user_id')) {
            return $this->redirectToRoute('home');
        }

        if ($request->getSession()->get('user_pseudo') != 'Minh') {
            return $this->redirectToRoute('home');
        }

        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $articleRepository->remove($article, true);
        }

        return $this->redirectToRoute('admin', [], Response::HTTP_SEE_OTHER);
    }
}
