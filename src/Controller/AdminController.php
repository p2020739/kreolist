<?php

namespace App\Controller;

use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    #[Route('/admin', name: 'admin')]
    public function index(Request $request, ArticleRepository $articleRepository, CategorieRepository $categorieRepository): Response
    {
        if (!$request->getSession()->get('user_id')) {
            return $this->redirectToRoute('home');
        }

        if ($request->getSession()->get('user_pseudo') != 'Minh') {
            return $this->redirectToRoute('home');
        }

        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
            'articles' => $articleRepository->findAll(),
            'categories' => $categorieRepository->findAll(),
        ]);
    }
}
