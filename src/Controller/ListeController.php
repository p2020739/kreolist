<?php

namespace App\Controller;

use App\Entity\Liste;
use App\Entity\Contenu;
use App\Form\AcheteType;
use App\Form\ListeType;
use App\Form\ContenuType;
use App\Repository\ListeRepository;
use App\Repository\UtilisateurRepository;
use App\Repository\ArticleRepository;
use App\Repository\ContenuRepository;
use App\Repository\CategorieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/liste')]
class ListeController extends AbstractController
{
    #[Route('/', name: 'liste.index', methods: ['GET'])]
    public function index(SessionInterface $session, UtilisateurRepository $utilisateurRepository, CategorieRepository $categorieRepository): Response
    {
        $tableau = array();
        $user = $utilisateurRepository->findOneBy(['id' => $session->get('user_id')]);
        if ($user == null) {
            return $this->redirectToRoute('home');
        }
        for($i = 0; $i < count($user->getListes()); $i++) {
            $tableau[$user->getListes()[$i]->getId()] = array(
                "nbValides" => 0,
                "nbTotal" => 0,
                "progression" => 0
            );
        }

        foreach($user->getListes() as $liste) {
            foreach($liste->getContenus() as $contenu) {
                $tableau[$liste->getId()]["nbTotal"]++;
                if($contenu->getAchete() == 1) {
                    $tableau[$liste->getId()]["nbValides"]++;
                }
            }
            if($tableau[$liste->getId()]["nbTotal"] != 0) {
                $tableau[$liste->getId()]["progression"] = round($tableau[$liste->getId()]["nbValides"] / $tableau[$liste->getId()]["nbTotal"] * 100);
            }
        }

        $stats = array(
            "nbListes" => count($user->getListes()),
            "totalSommes" => 0,
            "moySommes" => 0,
            "artPlusCher" => null,
            "artMoinsCher" => null,
            "repartition" => array()
        );

        foreach($categorieRepository->findAll() as $categorie) {
            $stats["repartition"][$categorie->getNom()] = 0;
        }

        foreach($user->getListes() as $liste) {
            $sommeliste = 0;
            foreach($liste->getContenus() as $contenu) {
                $sommeliste += $contenu->getArticle()->getPrix() * $contenu->getQuantite();
                if($stats["artPlusCher"] == null || $contenu->getArticle()->getPrix() > $stats["artPlusCher"]->getPrix()) {
                    $stats["artPlusCher"] = $contenu->getArticle();
                }
                if($stats["artMoinsCher"] == null || $contenu->getArticle()->getPrix() < $stats["artMoinsCher"]->getPrix()) {
                    $stats["artMoinsCher"] = $contenu->getArticle();
                }
                $stats["repartition"][$contenu->getArticle()->getCategorie()->getNom()] += $contenu->getArticle()->getPrix() * $contenu->getQuantite();
            }
            $stats["totalSommes"] += $sommeliste;
        }

        if(count($user->getListes()) != 0) {
            $stats["moySommes"] = round($stats["totalSommes"] / count($user->getListes()), 2);
        }
        
        return $this->render('liste/index.html.twig', [
            'listes' => $user->getListes(),
            'categories' => $categorieRepository->findAll(),
            'tableau' => $tableau,
            'stats' => $stats,
        ]);
    }

    #[Route('/new', name: 'app_liste_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ListeRepository $listeRepository, UtilisateurRepository $utilisateurRepository): Response
    {
        $user = $utilisateurRepository->findOneBy(['id' => $request->getSession()->get('user_id')]);

        $liste = new Liste();
        $liste->setUtilisateur($user);
        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $liste->setUtilisateur($user);
            $listeRepository->save($liste, true);

            return $this->redirectToRoute('liste.index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('liste/new.html.twig', [
            'liste' => $liste,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_liste_show', methods: ['GET', 'POST'])]
    public function show(Liste $liste, ArticleRepository $articleRepository, Request $request, ContenuRepository $contenuRepository): Response
    {
        $form = $this->createForm(ContenuType::class, null, [
            'articles' => $articleRepository->findAll(),
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // si l'id de l'article est déjà présent dans un contenu avec le meme id de liste, on met à jour la quantité du contenu avec l'ancienne + la nouvelle
            $contenuDejaExistant = $contenuRepository->findContenuParListeEtArticle($liste->getId(), $form->get('article')->getData()->getId());
            if ($contenuDejaExistant == null) {
                $contenu = new Contenu();
                $contenu->setListe($liste);
                $contenu->setArticle($form->get('article')->getData());
                $contenu->setQuantite($form->get('quantite')->getData());
                $contenu->setAchete(false);
                $contenuRepository->save($contenu, true);
                $liste->addContenu($contenu);
            } else {
                $contenuDejaExistant->setQuantite($contenuDejaExistant->getQuantite() + $form->get('quantite')->getData());
                $contenuDejaExistant->setAchete(false);
                $contenuRepository->save($contenuDejaExistant, true);
            }
        }

        $nbValides = 0;
        $nbTotal = 0;
        $coutAchete = 0;
        $coutTotal = 0;
        foreach ($liste->getContenus() as $contenu) {
            if ($contenu->getAchete()) {
                $nbValides++;
                $coutAchete += $contenu->getArticle()->getPrix() * $contenu->getQuantite();
            }
            $nbTotal++;
            $coutTotal += $contenu->getArticle()->getPrix() * $contenu->getQuantite();
        }
        if ($nbTotal > 0) {
            $pourcentage = $nbValides / $nbTotal * 100;
        } else {
            $pourcentage = 0;
        }

        return $this->render('liste/show.html.twig', [
            'liste' => $liste,
            'form' => $form->createView(),
            'nbValides' => $nbValides,
            'nbTotal' => $nbTotal,
            'pourcentage' => $pourcentage,
            'coutAchete' => $coutAchete,
            'coutTotal' => $coutTotal,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_liste_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Liste $liste, ListeRepository $listeRepository): Response
    {
        $form = $this->createForm(ListeType::class, $liste);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $listeRepository->save($liste, true);

            return $this->redirectToRoute('app_liste_show', ['id' => $liste->getId()]);
        }

        return $this->renderForm('liste/edit.html.twig', [
            'liste' => $liste,
            'form' => $form,
        ]);
    }

    #[Route('/delete/{id}', name: 'app_liste_delete', methods: ['POST'])]
    public function delete(Request $request, Liste $liste, ListeRepository $listeRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$liste->getId(), $request->request->get('_token'))) {
            $listeRepository->remove($liste, true);
        }

        return $this->redirectToRoute('liste.index', [], Response::HTTP_SEE_OTHER);
    }
}
