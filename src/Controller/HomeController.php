<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Form\ConnexionType;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(Request $request, UtilisateurRepository $repository, EntityManagerInterface $entityManager): Response
    {
        if ($request->getSession()->get('user_id')) {
            return $this->redirectToRoute('liste.index');
        }
        $form = $this->createForm(ConnexionType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            // traiter le formulaire 
            $user = $repository->findOneBy(['pseudo' => $form->getData()->getPseudo()]);
            if ($user) {
                // instancier la session et y stocker l'utilisateur
                $session = $request->getSession();
                $session->set('user_id', $user->getId());
                $session->set('user_pseudo', $user->getPseudo());
                $this->addFlash('success', 'Bienvenue ' . $user->getPseudo());
                return $this->redirectToRoute('liste.index');
            } else {
                // créer l'utilisateur dans la base de données et l'instancier dans la session
                $user = new Utilisateur();
                $user->setPseudo($form->getData()->getPseudo());
                $entityManager->persist($user);
                $entityManager->flush();
                $session = $request->getSession();
                $session->set('user_id', $user->getId());
                $session->set('user_pseudo', $user->getPseudo());
            
                $this->addFlash('danger', 'Utilisateur inconnu');
                return $this->redirectToRoute('home');
            }
            // return $this->redirectToRoute('service.index');
        }
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'form' => $form->createView()
        ]);
    }

    #[Route('/deconnexion', name: 'deconnexion')]
    public function deconnexion(Request $request): Response
    {
        $session = $request->getSession();
        $session->clear();
        return $this->redirectToRoute('home');
    }
}
