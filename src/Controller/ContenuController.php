<?php

namespace App\Controller;

use App\Entity\Contenu;
use App\Form\Contenu1Type;
use App\Repository\ContenuRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/contenu')]
class ContenuController extends AbstractController
{
    #[Route('/', name: 'app_contenu_index', methods: ['GET'])]
    public function index(ContenuRepository $contenuRepository): Response
    {
        return $this->render('contenu/index.html.twig', [
            'contenus' => $contenuRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_contenu_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ContenuRepository $contenuRepository): Response
    {
        $contenu = new Contenu();
        $form = $this->createForm(Contenu1Type::class, $contenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // si l'id de l'article est déjà présent dans un contenu avec le meme id de liste, on met à jour la quantité du contenu avec l'ancienne + la nouvelle
            $contenuDejaExistant = $contenuRepository->findContenuParListeEtArticle($contenu->getListe()->getId(), $contenu->getArticle()->getId());
            dump($contenuDejaExistant);
            if ($contenuDejaExistant == null) {
                $contenu->setAchete(0);
                $contenuRepository->save($contenu, true);
            } else {
                $contenuDejaExistant->setQuantite($contenuDejaExistant->getQuantite() + $contenu->getQuantite());
                $contenuDejaExistant->setAchete(0);
                $contenuRepository->save($contenuDejaExistant, true);
            }
            return $this->redirectToRoute('app_contenu_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contenu/new.html.twig', [
            'contenu' => $contenu,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_contenu_show', methods: ['GET'])]
    public function show(Contenu $contenu): Response
    {
        return $this->render('contenu/show.html.twig', [
            'contenu' => $contenu,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_contenu_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Contenu $contenu, ContenuRepository $contenuRepository): Response
    {

        $liste = $contenu->getListe();
        $article = $contenu->getArticle();
        
        $form = $this->createForm(Contenu1Type::class, $contenu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contenu->setListe($liste);
            $contenu->setArticle($article);
            $contenuRepository->save($contenu, true);

            return $this->redirectToRoute('app_liste_show', array('id'=>$liste->getId()), Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('contenu/edit.html.twig', [
            'contenu' => $contenu,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_contenu_delete', methods: ['POST'])]
    public function delete(Request $request, Contenu $contenu, ContenuRepository $contenuRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contenu->getId(), $request->request->get('_token'))) {
            $contenuRepository->remove($contenu, true);
        }

        return $this->redirectToRoute('app_liste_show', array('id'=>$contenu->getListe()->getId()), Response::HTTP_SEE_OTHER);
    }

    #[Route('/{id}/delete', name: 'app_contenu_changeetat', methods: ['GET', 'POST'])]
    public function switchState(Request $request, Contenu $contenu, ContenuRepository $contenuRepository): Response
    {
        if ($this->isCsrfTokenValid('switch'.$contenu->getId(), $request->request->get('_token'))) {
            $contenu->setAchete($contenu->getAchete() == 0 ? 1 : 0);
            $contenuRepository->save($contenu, true);
        }

        return $this->redirectToRoute('app_liste_show', array('id'=>$contenu->getListe()->getId()), Response::HTTP_SEE_OTHER);
    }
}
