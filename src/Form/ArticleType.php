<?php

namespace App\Form;

use App\Entity\Article;
use App\Entity\Categorie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;

class ArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('libelle', TextType::class, [
                'label' => 'Libellé',
                'attr' => [
                    'class' => 'form-control w-50',
                ],
                'label_attr' => [
                    'class' => 'form-label mt-4',
                ],
            ])
            ->add('prix', MoneyType::class, [
                'label' => 'Prix (€)',
                'currency' => '',
                'attr' => [
                    'class' => 'form-control w-50',
                ],
                'label_attr' => [
                    'class' => 'form-label mt-4',
                ],
            ])
            ->add('categorie', ChoiceType::class, [
                'label' => 'Catégorie',
                'choices' => $options['categories'],
                'choice_label' => function (?Categorie $entity) {
                    return $entity ? $entity->getNom() : '';
                },
                'choice_value' => function (?Categorie $entity) {
                    return $entity ? $entity->getId() : '';
                },
                'attr' => [
                    'class' => 'form-select w-50',
                ],
                'label_attr' => [
                    'class' => 'form-label mt-4',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Article::class,
            'categories' => [],
        ]);
    }
}
