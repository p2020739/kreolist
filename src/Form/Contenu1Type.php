<?php

namespace App\Form;

use App\Entity\Contenu;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class Contenu1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quantite', IntegerType::class, [
                'label' => 'Quantité',
                'attr' => [
                    'class' => 'form-control',
                ],
                'label_attr' => [
                    'class' => 'form-label mt-4',
                ],
            ])
            ->add('liste', TextType::class, [
                'label' => 'Liste',
                'attr' => [
                    'class' => 'form-control',
                    'disabled' => 'disabled',
                ],
                'label_attr' => [
                    'class' => 'form-label mt-4',
                ],
            ])
            ->add('article', TextType::class, [
                'label' => 'Article',
                'attr' => [
                    'class' => 'form-control',
                    'disabled' => 'disabled',
                ],
                'label_attr' => [
                    'class' => 'form-label mt-4',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-success mt-4',
                ],
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contenu::class,
            'listes' => null,
            'articles' => null,
        ]);
    }
}
